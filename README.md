# I went to Rails Girls workshop

Presentation for IT Lightning Talks: session #15 (CERN, Geneva, 26th January 2018)

To view the slides in the browser, go [here](https://switowski.gitlab.io/railsgirls-writeup/)

You can see the videos of the presentation [CERN Document Server](https://cds.cern.ch/record/2302078)

PDF version of the slides is available in the *Slides.pdf* file (but slides look way better in the browser)

## Important

This repository will no longer be updated.
